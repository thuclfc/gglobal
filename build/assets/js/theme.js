/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2022. MIT licensed.
 */(function ($) {
  "use strict"; //Submenu Dropdown Toggle

  if ($('.main-nav__main-navigation li.dropdown ul').length) {
    $('.main-nav__main-navigation li.dropdown').append('<button class="dropdown-btn"><i class="fa fa-angle-right"></i></button>');
  }

  function dynamicCurrentMenuClass(selector) {
    let FileName = window.location.href.split('/').reverse()[0];
    selector.find('li').each(function () {
      let anchor = $(this).find('a');

      if ($(anchor).attr('href') == FileName) {
        $(this).addClass('current');
      }
    }); // if any li has .current elmnt add class

    selector.children('li').each(function () {
      if ($(this).find('.current').length) {
        $(this).addClass('current');
      }
    }); // if no file name return

    if ('' == FileName) {
      selector.find('li').eq(0).addClass('current');
    }
  } // mobile menu


  if ($('.main-nav__main-navigation').length) {
    let mobileNavContainer = $('.mobile-nav__container');
    let mainNavContent = $('.main-nav__main-navigation').html();
    mobileNavContainer.append(function () {
      return mainNavContent;
    }); //Dropdown Button

    mobileNavContainer.find('li.dropdown .dropdown-btn').on('click', function () {
      $(this).toggleClass('open');
      $(this).prev('ul').slideToggle(500);
    }); // dynamic current class

    let mainNavUL = $('.main-nav__main-navigation').find('.main-nav__navigation-box');
    let mobileNavUL = mobileNavContainer.find('.main-nav__navigation-box');
    dynamicCurrentMenuClass(mainNavUL);
    dynamicCurrentMenuClass(mobileNavUL);
  }

  if ($('.datepicker').length) {
    $('.datepicker').datepicker();
  }

  if ($('.counter').length) {
    $('.counter').counterUp({
      delay: 10,
      time: 1500
    });
  }

  if ($('.img-popup').length) {
    var groups = {};
    $('.img-popup').each(function () {
      var id = parseInt($(this).attr('data-group'), 10);

      if (!groups[id]) {
        groups[id] = [];
      }

      groups[id].push(this);
    });
    $.each(groups, function () {
      $(this).magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        gallery: {
          enabled: true
        }
      });
    });
  }

  ;

  if ($('.wow').length) {
    var wow = new WOW({
      boxClass: 'wow',
      // animated element css class (default is wow)
      animateClass: 'animated',
      // animation css class (default is animated)
      mobile: true,
      // trigger animations on mobile devices (default is true)
      live: true // act on asynchronously loaded content (default is true)

    }); //wow.init();
  }

  if ($('.video-popup').length) {
    $('.video-popup').magnificPopup({
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: true,
      fixedContentPos: false
    });
  }

  if ($('[data-toggle="tooltip"]').length) {
    $('[data-toggle="tooltip"]').tooltip();
  }

  if ($('.stricky').length) {
    $('.stricky').addClass('original').clone(true).insertAfter('.stricky').addClass('stricked-menu').removeClass('original');
  }

  if ($('.scroll-to-target').length) {
    $(".scroll-to-target").on('click', function () {
      var target = $(this).attr('data-target'); // animate

      $('html, body').animate({
        scrollTop: $(target).offset().top
      }, 1000);
      return false;
    });
  }

  if ($('.side-menu__toggler').length) {
    $('.side-menu__toggler').on('click', function (e) {
      $('.side-menu__block').toggleClass('active');
      e.preventDefault();
    });
  }

  if ($('.side-menu__block-overlay').length) {
    $('.side-menu__block-overlay').on('click', function (e) {
      $('.side-menu__block').removeClass('active');
      e.preventDefault();
    });
  }

  if ($('.side-content__toggler').length) {
    $('.side-content__toggler').on('click', function (e) {
      $('.side-content__block').toggleClass('active');
      e.preventDefault();
    });
  }

  if ($('.side-content__block-overlay').length) {
    $('.side-content__block-overlay').on('click', function (e) {
      $('.side-content__block').removeClass('active');
      e.preventDefault();
    });
  }

  if ($('.search-popup__toggler').length) {
    $('.search-popup__toggler').on('click', function (e) {
      $('.search-popup').addClass('active');
      e.preventDefault();
    });
  }

  if ($('.search-popup__overlay').length) {
    $('.search-popup__overlay').on('click', function (e) {
      $('.search-popup').removeClass('active');
      e.preventDefault();
    });
  }

  $(window).on('scroll', function () {
    if ($('.scroll-to-top').length) {
      var strickyScrollPos = 100;

      if ($(window).scrollTop() > strickyScrollPos) {
        $('.scroll-to-top').fadeIn(500);
      } else if ($(this).scrollTop() <= strickyScrollPos) {
        $('.scroll-to-top').fadeOut(500);
      }
    }

    if ($('.stricked-menu').length) {
      var headerScrollPos = 100;
      var stricky = $('.stricked-menu');

      if ($(window).scrollTop() > headerScrollPos) {
        stricky.addClass('stricky-fixed');
      } else if ($(this).scrollTop() <= headerScrollPos) {
        stricky.removeClass('stricky-fixed');
      }
    }
  });

  if ($('.thm__owl-carousel').length) {
    $('.thm__owl-carousel').each(function () {
      var Self = $(this);
      var carouselOptions = Self.data('options');
      var carouselPrevSelector = Self.data('carousel-prev-btn');
      var carouselNextSelector = Self.data('carousel-next-btn');
      var carouselDotsContainer = Self.data('carousel-dots-container');
      var thmCarousel = Self.owlCarousel(carouselOptions);

      if (carouselPrevSelector !== undefined) {
        $(carouselPrevSelector).on('click', function () {
          thmCarousel.trigger('prev.owl.carousel', [1000]);
          return false;
        });
      }

      if (carouselNextSelector !== undefined) {
        $(carouselNextSelector).on('click', function () {
          thmCarousel.trigger('next.owl.carousel', [1000]);
          return false;
        });
      }

      if (carouselDotsContainer !== undefined) {
        $(carouselDotsContainer).find('.owl-dot').on('click', function () {
          var dotIndex = $(this).index(); // $(carouselDotsContainer).find('.owl-dot').removeClass('active');
          // $(carouselDotsContainer).find('.owl-dot').eq(dotIndex).addClass('active');

          thmCarousel.trigger('to.owl.carousel', [dotIndex, 1000]);
        });
      }
    });
  }

  $(window).on('load', function () {
    if ($('.preloader').length) {
      $('.preloader').fadeOut('slow');
    }
  });
  var urlcurrent = window.location.pathname;
  $(".main-nav__navigation-box li a[href$='" + urlcurrent + "']").addClass('active');
})(jQuery);