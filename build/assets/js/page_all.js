/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2022. MIT licensed.
 */$(document).ready(function () {
  //show menu
  $('.navbar-toggler').on('click', function () {
    $(this).toggleClass('active');
    $('.navbar-collapse').toggleClass('show');
  });
  $('.navbar-collapse .close').on('click', function () {
    $('.navbar-collapse').removeClass('show');
  }); // active navbar of page current

  var urlcurrent = window.location.href;
  $(".navbar-nav li a[href$='" + urlcurrent + "'],.nav-category li a[href$='" + urlcurrent + "']").addClass('active');
  $(window).on("load", function (e) {
    $(".navbar-nav .sub-menu").parent("li").append("<span class='show-menu'></span>");
  });
  $('.navbar-nav > li').click(function () {
    $(this).toggleClass('active');
  });
  $('.btn_search').on('click', function () {
    $('.form-search input').focus();
    $('.form-search').toggleClass('show');
  });
});